package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.mapping.Array;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project extends AbstractOwnerEntity {
    @Column
    @NonNull
    private String name;
    @Column
    @Nullable
    private String description;
    @NonNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;
    @Nullable
    @Column(name = "start_date")
    private Date startDate;
    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;
    @NonNull
    @Column(name = "created")
    private Date created = new Date();
    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
