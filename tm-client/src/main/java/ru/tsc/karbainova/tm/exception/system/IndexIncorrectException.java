package ru.tsc.karbainova.tm.exception.system;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {
    public IndexIncorrectException() {
        super("Error Index is incorrect.");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error Value " + value + " is not number.");
    }
}
